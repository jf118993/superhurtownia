!macro customInstall
    MessageBox MB_YESNO|MB_ICONQUESTION "Czy chcesz zainstalować lokalną bazę danych MySQL?" IDYES true IDNO false
    true:
        File /oname=$PLUGINSDIR\xampp.exe "${BUILD_RESOURCES_DIR}\xampp.exe"
        ExecWait '"$PLUGINSDIR\xampp.exe" --unattendedmodeui minimal --mode unattended --disable-components xampp_filezilla,xampp_mercury,xampp_tomcat,xampp_perl,xampp_webalizer,xampp_sendmail'
    false:
!macroend