interface User {
    name: string,
    roles: string[]
    //TODO coś jeszcze?
}

let globalUser: User | null = null;

export function setUser(user: User | null): void {
    globalUser = user;
}

export function getUser() {
    return globalUser;
}

export function hasRole(role: string): boolean {
    const user: User | null = getUser();
    if (user == null) {
        return false;
    }
    return user.roles.includes(role);
}
//TODO czemu tutaj nie może być hasRole zamiast definiowania tego w każdym komponencie