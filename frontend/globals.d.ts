export {};

declare global {
    interface Window {
        jwtToken: string | null,
        backendPort: string
    }
}