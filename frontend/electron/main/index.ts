import {app, BrowserWindow, shell, ipcMain, dialog} from 'electron'
import {release} from 'node:os'
import {join} from 'node:path'
import * as path from "path";
import net from "net"
import childProcess from "child_process"
import fs from 'fs'

let backendPort: string

async function getFreePort() {
    return new Promise<string>( resolve => {
        const srv = net.createServer();
        srv.listen(0, () => {
            const serverInfo = srv.address()
            let port = ""
            if (typeof serverInfo === "string")
                port = serverInfo.split(":")[1]
            else
                port = serverInfo.port.toString()
            srv.close((_) => resolve(port))
        });
    })
}

// The built directory structure
//
// ├─┬ dist-electron
// │ ├─┬ main
// │ │ └── index.js    > Electron-Main
// │ └─┬ preload
// │   └── index.js    > Preload-Scripts
// ├─┬ dist
// │ └── index.html    > Electron-Renderer
//
process.env.DIST_ELECTRON = join(__dirname, '..')
process.env.DIST = join(process.env.DIST_ELECTRON, '../dist')
process.env.PUBLIC = process.env.VITE_DEV_SERVER_URL
    ? join(process.env.DIST_ELECTRON, '../public')
    : process.env.DIST

// Disable GPU Acceleration for Windows 7
if (release().startsWith('6.1')) app.disableHardwareAcceleration()

// Set application name for Windows 10+ notifications
if (process.platform === 'win32') app.setAppUserModelId(app.getName())

if (!app.requestSingleInstanceLock()) {
    app.quit()
    process.exit(0)
}

// Remove electron security warnings
// This warning only shows in development mode
// Read more on https://www.electronjs.org/docs/latest/tutorial/security
// process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'

let win: BrowserWindow | null = null
// Here, you can also use other preload
const preload = join(__dirname, '../preload/index.js')
const url = process.env.VITE_DEV_SERVER_URL
const indexHtml = join(process.env.DIST, 'index.html')

async function createWindow() {
    win = new BrowserWindow({
        title: 'Main window',
        icon: join(process.env.PUBLIC, 'favicon.ico'),
        webPreferences: {
            preload,
            // Warning: Enable nodeIntegration and disable contextIsolation is not secure in production
            // Consider using contextBridge.exposeInMainWorld
            // Read more on https://www.electronjs.org/docs/latest/tutorial/context-isolation
            nodeIntegration: true,
            contextIsolation: false,
        },
    })

    if (process.env.VITE_DEV_SERVER_URL) { // electron-vite-vue#298
        win.loadURL(url)
        // Open devTool if the app is not packaged
        win.webContents.openDevTools()
    } else {
        win.loadFile(indexHtml)
    }

    // Test actively push message to the Electron-Renderer
    win.webContents.on('did-finish-load', () => {
        win?.webContents.send('main-process-message', new Date().toLocaleString())
    })

    // Make all links open with the browser, not with the application
    win.webContents.setWindowOpenHandler(({url}) => {
        if (url.startsWith('https:')) shell.openExternal(url)
        return {action: 'deny'}
    })
    // win.webContents.on('will-navigate', (event, url) => { }) #344
}

let backendProcess
app.whenReady().then(() => {
    createWindow()
})

app.on('window-all-closed', () => {
    try {
        fs.accessSync('C:\\xampp\\xampp_stop.exe')
        childProcess.spawn('C:\\xampp\\xampp_stop.exe', [], {detached: true})
    } catch (e) {
        console.log('XAMPP is not running')
    }
    win = null
    if (process.platform !== 'darwin') app.quit()
})

app.on('second-instance', () => {
    if (win) {
        // Focus on the main window if the user tried to open another
        if (win.isMinimized()) win.restore()
        win.focus()
    }
})

app.on('activate', () => {
    const allWindows = BrowserWindow.getAllWindows()
    if (allWindows.length) {
        allWindows[0].focus()
    } else {
        createWindow()
    }
})

// New window example arg: new windows url
ipcMain.handle('open-win', (_, arg) => {
    const childWindow = new BrowserWindow({
        webPreferences: {
            preload,
            nodeIntegration: true,
            contextIsolation: false,
        },
    })

    if (process.env.VITE_DEV_SERVER_URL) {
        childWindow.loadURL(`${url}#${arg}`)
    } else {
        childWindow.loadFile(indexHtml, {hash: arg})
    }
})

ipcMain.on('show-dialog', (event, orderId: number, jwtToken: string) => {
    dialog.showSaveDialog({
        title: 'Zapisz fakturę',
        defaultPath: 'faktura.pdf',
        filters: [
            {name: 'PDF', extensions: ['pdf']}
        ]
    }).then((result) => {
        if (!result.canceled) {
            fetch(`http://localhost:${backendPort}/api/create-pdf`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${jwtToken}`
                },
                body: JSON.stringify({
                    path: result.filePath,
                    orderId: orderId
                })
            }).then((response) => {
                if (!response.ok) {
                    throw new Error("Network response was not ok");
                }
            })
        }
    }).catch((err) => {
        console.error(err)
    })
})

ipcMain.on('run-backend', async (_, args: {
    localDb: boolean,
    dbIp: string,
    dbPort: string,
    dbName: string,
    dbUsername: string,
    dbPassword: string,
}) => {
    if (args.localDb) {
        try {
            fs.accessSync('C:\\xampp\\mysql_start.bat')
            childProcess.exec('C:\\xampp\\mysql_start.bat')
        } catch (e) {
            dialog.showErrorBox('Błąd', 'Nie znaleziono lokalnej bazy danych!')
            win?.webContents.send('backend-stopped')
            return
        }
    }

    const datasourceUrl = `jdbc:mariadb://${args.dbIp}:${args.dbPort}/${args.dbName}?createDatabaseIfNotExist=true`
    const datasourceUsername = args.dbUsername
    const datasourcePassword = args.dbPassword
    backendPort = await getFreePort()

    let javaPath: string
    let jarPath: string

    if (__dirname.includes('.asar')) {
        javaPath = path.join(__dirname, '../../../../backendRuntime/bin/java')
        jarPath = path.join(__dirname, '../../../../backend.jar')
    } else {
        javaPath = path.join(app.getAppPath(), 'backendRuntime/bin/java')
        jarPath = path.join(app.getAppPath(), 'backend.jar')
    }

    const javaArgs = [
        '-jar',
        jarPath,
        `--spring.datasource.url=${datasourceUrl}`,
        `--spring.datasource.username=${datasourceUsername}`,
        `--spring.datasource.password=${datasourcePassword}`,
        `--server.port=${backendPort}`
    ]

    backendProcess = childProcess.spawn(javaPath, javaArgs, {})
    backendProcess.stdout.on('data', (data) => {
        if (data.toString().includes('Started BackendApplication')) {
            console.log('Backend started')
            win?.webContents.send('backend-started', backendPort)
        }
        process.stdout.write(data)
    })
    backendProcess.on('exit', (code, _) => {
        console.log('Backend stopped')
        if (code == 0)
            dialog.showErrorBox('Utracono połączenie!', `Proces backendu został zatrzymany z kodem ${code}!`)

        win?.webContents.send('backend-stopped')
    })
})

ipcMain.on('disconnect', () => {
    backendProcess.kill()
})