package pl.superhurtownia.backend;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.superhurtownia.backend.repositories.RoleRepository;
import pl.superhurtownia.backend.repositories.UserRepository;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class BackendApplicationTests {

	@Test
	void contextLoads(@Autowired UserRepository userRepository, @Autowired RoleRepository roleRepository) {
		assertTrue(userRepository.findByName("admin").isPresent());
		assertTrue(roleRepository.findByName("ROLE_ADMIN").isPresent());
		assertTrue(roleRepository.findByName("ROLE_OFFICE").isPresent());
		assertTrue(roleRepository.findByName("ROLE_MANAGER").isPresent());
		assertTrue(roleRepository.findByName("ROLE_WAREHOUSE").isPresent());
	}

}
