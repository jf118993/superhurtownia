package pl.superhurtownia.backend.services;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import pl.superhurtownia.backend.models.Role;
import pl.superhurtownia.backend.models.User;
import pl.superhurtownia.backend.repositories.UserRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class AuthServiceTest {
    PasswordEncoder noOpPasswordEncoder = new PasswordEncoder() {
        @Override
        public String encode(CharSequence rawPassword) {
            return rawPassword.toString();
        }

        @Override
        public boolean matches(CharSequence rawPassword, String encodedPassword) {
            return rawPassword.equals(encodedPassword);
        }
    };
    UserRepository mockUserRepository = new UserRepository() {
        @Override
        public Optional<User> findByName(String name) {
            return Optional.of(new User() {{
                setName(name);
                setPassword(noOpPasswordEncoder.encode(name));
                setRoles(List.of(new Role() {{
                    setName("ROLE_ADMIN");
                }}));
            }});
        }

        @Override
        public <S extends User> S save(S entity) {
            throw new NotImplementedException();
        }

        @Override
        public <S extends User> Iterable<S> saveAll(Iterable<S> entities) {
            throw new NotImplementedException();
        }

        @Override
        public Optional<User> findById(Long aLong) {
            throw new NotImplementedException();
        }

        @Override
        public boolean existsById(Long aLong) {
            throw new NotImplementedException();
        }

        @Override
        public Iterable<User> findAll() {
            throw new NotImplementedException();
        }

        @Override
        public Iterable<User> findAllById(Iterable<Long> longs) {
            throw new NotImplementedException();
        }

        @Override
        public long count() {
            throw new NotImplementedException();
        }

        @Override
        public void deleteById(Long aLong) {
            throw new NotImplementedException();
        }

        @Override
        public void delete(User entity) {
            throw new NotImplementedException();
        }

        @Override
        public void deleteAllById(Iterable<? extends Long> longs) {
            throw new NotImplementedException();
        }

        @Override
        public void deleteAll(Iterable<? extends User> entities) {
            throw new NotImplementedException();
        }

        @Override
        public void deleteAll() {
            throw new NotImplementedException();
        }

        @Override
        public List<User> findByRolesContaining(Role role) {
            throw new NotImplementedException();
        }
    };

    @Test
    void generateToken(@Autowired JwtEncoder jwtEncoder) {
        var authService = new AuthService(jwtEncoder, noOpPasswordEncoder, mockUserRepository);

        var token = authService.generateToken("user", "user");

        assertFalse(token.isEmpty());
    }
}