create table batches_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table clients
(
    id         bigint       not null
        primary key,
    address    varchar(255) not null,
    name       varchar(255) not null,
    NIP_number bigint       null
);

create table clients_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table inbound_orders_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table outbound_orders_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table product_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table products
(
    id         bigint        not null
        primary key,
    cost       double(20, 2) not null,
    created_at datetime(6)   null,
    name       varchar(255)  not null,
    updated_at datetime(6)   null,
    country    varchar(255)  null
);

create table batches
(
    id              bigint      not null
        primary key,
    amount          double      not null,
    created_at      datetime(6) null,
    delivery_date   datetime(6) not null,
    expiration_date datetime(6) not null,
    updated_at      datetime(6) null,
    product_id      bigint      null,
    constraint FKjb38v1mk479a6t6ay2mewo03m
        foreign key (product_id) references products (id)
);

create table inbound_orders
(
    id         bigint      not null
        primary key,
    amount     double      not null,
    created_at datetime(6) null,
    order_date date        null,
    status     smallint    null,
    updated_at datetime(6) null,
    product_id bigint      null,
    constraint FK3hwcpyw23mt3n6row3sk9ooik
        foreign key (product_id) references products (id)
);

create table outbound_orders
(
    id         bigint        not null
        primary key,
    amount     double        not null,
    cost       double(20, 2) not null,
    created_at datetime      null,
    order_date date          null,
    status     smallint      null,
    updated_at datetime(6)   null,
    client_id  bigint        null,
    product_id bigint        null,
    constraint FK4tp42oweyvw2978gmf6630es
        foreign key (client_id) references clients (id),
    constraint FK98p9nsuom0fdf3uqbhjhoy7np
        foreign key (product_id) references products (id)
);

create table products_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table roles
(
    id         bigint       not null
        primary key,
    created_at datetime(6)  null,
    name       varchar(255) not null,
    updated_at datetime(6)  null
);

create table roles_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table tasks_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table users
(
    id         bigint       not null
        primary key,
    created_at datetime(6)  null,
    name       varchar(255) not null,
    password   varchar(255) not null,
    updated_at datetime(6)  null,
    constraint UK_3g1j96g94xpk3lpxl2qbl985x
        unique (name)
);

create table users_roles
(
    users_id bigint not null,
    roles_id bigint not null,
    constraint FKa62j07k5mhgifpp955h37ponj
        foreign key (roles_id) references roles (id),
    constraint FKml90kef4w2jy7oxyqv742tsfc
        foreign key (users_id) references users (id)
);

create table users_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table warehouse_teams
(
    id         bigint       not null
        primary key,
    created_at datetime(6)  null,
    name       varchar(255) null,
    updated_at datetime(6)  null
);

create table tasks
(
    id         bigint      not null
        primary key,
    created_at datetime(6) null,
    status     smallint    null,
    updated_at datetime(6) null,
    order_id   bigint      null,
    team_id    bigint      null,
    constraint FK7ttr7ol9p7duwmuja12dw7k1l
        foreign key (team_id) references warehouse_teams (id),
    constraint FKgack837a23btocw9e6jyy5p6v
        foreign key (order_id) references outbound_orders (id)
);

create table warehouse_teams_seq
(
    next_not_cached_value bigint(21)          not null,
    minimum_value         bigint(21)          not null,
    maximum_value         bigint(21)          not null,
    start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
    increment             bigint(21)          not null comment 'increment value',
    cache_size            bigint(21) unsigned not null,
    cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
    cycle_count           bigint(21)          not null comment 'How many cycles have been done'
);

create table warehouse_teams_tasks
(
    warehouse_teams_id bigint not null,
    tasks_id           bigint not null,
    constraint UK_9detx5vip8oleym27jabpu683
        unique (tasks_id),
    constraint FKmx1t1jyfmyc4mmula0p38moy6
        foreign key (warehouse_teams_id) references warehouse_teams (id),
    constraint FKqy3v9jxao5g8tkeflcvl163jf
        foreign key (tasks_id) references tasks (id)
);

create table warehouse_teams_workers
(
    warehouse_teams_id bigint not null,
    workers_id         bigint not null,
    constraint FKi8px18fjka9dc021n4xhntfm2
        foreign key (warehouse_teams_id) references warehouse_teams (id),
    constraint FKm8oxdthfl4t3579m6f01456qg
        foreign key (workers_id) references users (id)
);

