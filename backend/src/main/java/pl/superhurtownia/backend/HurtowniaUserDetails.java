package pl.superhurtownia.backend;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.superhurtownia.backend.models.Role;
import pl.superhurtownia.backend.models.User;

import java.util.Collection;
import java.util.List;

/**
 * Klasa danych użytkownika
 */
public class HurtowniaUserDetails implements UserDetails {
    private final User user;
    private final List<GrantedAuthority> authorities;

    /**
     * Konstruktor danych użytkownika
     * @param user użytkownik
     * @param roles role użytkownika
     */
    public HurtowniaUserDetails(User user, List<Role> roles) {
        this.user = user;
        authorities = roles.stream()
                .map(role -> (GrantedAuthority) new SimpleGrantedAuthority(role.getName()))
                .toList();
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
