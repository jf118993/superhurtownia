package pl.superhurtownia.backend.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * Rekord właściwości klucza RSA
 * @param publicKey Klucz publiczny
 * @param privateKey Klucz prywatny
 */
@ConfigurationProperties(prefix = "rsa")
public record RsaKeyProperties(RSAPublicKey publicKey, RSAPrivateKey privateKey) {
}
