package pl.superhurtownia.backend.configurations;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Klasa konfiguracyjna zasad zabezpieczeń.
 */
@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfig implements WebMvcConfigurer {

    private final RsaKeyProperties rsaKeys;

    /**
     * Konstruktor klasy konfiguracyjnej zasad zabezpieczeń
     * @param rsaKeys Klucze RSA
     */
    @Autowired
    public SecurityConfig(RsaKeyProperties rsaKeys) {
        this.rsaKeys = rsaKeys;
    }

    /**
     * Metoda zwracająca enkoder haseł
     * @return Enkoder haseł
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Metoda zwracająca dekoder JWT
     * @return Dekoder JWT
     */
    @Bean
    public JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withPublicKey(rsaKeys.publicKey()).build();
    }

    /**
     * Metoda zwracająca enkoder JWT
     * @return Enkoder JWT
     */
    @Bean
    public JwtEncoder jwtEncoder() {
        var jwk = new RSAKey.Builder(rsaKeys.publicKey()).privateKey(rsaKeys.privateKey()).build();
        var jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
        return new NimbusJwtEncoder(jwks);
    }

    /**
     * Metoda zwracająca konfigurację CORS
     * @return Konfiguracja CORS
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("http://localhost:5173");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:5173")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH")
                .allowedHeaders("*");
    }


    /**
     * Metoda zwracająca łańcuch filtrów zabezpieczeń dla logowania
     * @param http Obiekt konfiguracji zabezpieczeń
     * @return Łańcuch filtrów zabezpieczeń
     * @throws Exception Wyjątek rzucany w przypadku błędu konfiguracji
     */
    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Bean
    SecurityFilterChain tokenSecurityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf().disable()
                .cors().and()
                .securityMatcher(new AntPathRequestMatcher("/api/auth/token"))
                .authorizeHttpRequests()
                    .anyRequest().permitAll()
                .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                    .authenticationEntryPoint(new BearerTokenAuthenticationEntryPoint())
                    .accessDeniedHandler(new BearerTokenAccessDeniedHandler())
                .and()
                .build();
    }

    /**
     * Metoda zwracająca główny łańcuch filtrów zabezpieczeń
     * @param http Obiekt konfiguracji zabezpieczeń
     * @return Łańcuch filtrów zabezpieczeń
     * @throws Exception Wyjątek rzucany w przypadku błędu konfiguracji
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf().disable()
                .cors().and()
                .authorizeHttpRequests()
                    .requestMatchers("/api/clients/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_OFFICE", "SCOPE_ROLE_WAREHOUSE", "SCOPE_ROLE_MANAGER")
                    .requestMatchers("/api/inbound-orders/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_OFFICE")
                    .requestMatchers("/api/batches/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_OFFICE", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/products/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_OFFICE", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/outbound-orders/**").hasAnyAuthority("SCOPE_ROLE_ADMIN","SCOPE_ROLE_OFFICE", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/order-groups/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_OFFICE", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/tasks/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/tasks-team**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/tasks-with-teams").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_MANAGER", "SCOPE_ROLE_WAREHOUSE")
                    .requestMatchers("/api/warehouse-view/**").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_OFFICE")
                    .requestMatchers("/api/create-pdf").hasAnyAuthority("SCOPE_ROLE_ADMIN", "SCOPE_ROLE_OFFICE")
                    .requestMatchers("/api/**").hasAuthority("SCOPE_ROLE_ADMIN")
                    .anyRequest().authenticated()
                .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                .exceptionHandling()
                    .authenticationEntryPoint(new BearerTokenAuthenticationEntryPoint())
                    .accessDeniedHandler(new BearerTokenAccessDeniedHandler())
                .and()
                .build();
    }

}
