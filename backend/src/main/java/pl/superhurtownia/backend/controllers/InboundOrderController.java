package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.InboundOrder;
import pl.superhurtownia.backend.repositories.InboundOrderRepository;

/**
 * Kontroler zamówień przychodzących
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class InboundOrderController {
    private final InboundOrderRepository inboundOrderRepository;

    /**
     * Konstruktor kontrolera zamówień przychodzących
     * @param inboundOrderRepository Repozytorium zamówień przychodzących
     */
    @Autowired
    public InboundOrderController(InboundOrderRepository inboundOrderRepository) {
        this.inboundOrderRepository = inboundOrderRepository;
    }

    /**
     * Zwraca wszystkie zamówienia przychodzące
     * @return 200 lista wszystkich zamówień przychodzących
     */
    @GetMapping("/api/inbound-orders")
    public Iterable<InboundOrder> getAll() {
        return inboundOrderRepository.findAll();
    }

    /**
     * Zwraca zamówienie przychodzące o podanym id
     * @param id id zamówienia przychodzącego
     * @return 200 zamówienie przychodzące o podanym id
     */
    @GetMapping("/api/inbound-orders/{id}")
    public InboundOrder get(@PathVariable Long id) {
        return inboundOrderRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Tworzy nowe zamówienie przychodzące
     * @param inboundOrder dane nowego zamówienia przychodzącego
     * @return 201 dane utworzonego zamówienia przychodzącego
     */
    @PostMapping("/api/inbound-orders")
    @ResponseStatus(HttpStatus.CREATED)
    public InboundOrder create(@RequestBody InboundOrder inboundOrder) {
        inboundOrder.setId(null);
        return inboundOrderRepository.save(inboundOrder);
    }

    /**
     * Usuwa zamówienie przychodzące o podanym id
     * @param id id zamówienia przychodzącego
     */
    @DeleteMapping("/api/inbound-orders/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        inboundOrderRepository.deleteById(id);
    }

    /**
     * Aktualizuje zamówienie przychodzące o podanym id
     * @param inboundOrder dane zamówienia przychodzącego
     * @param id id zamówienia przychodzącego
     * @return 200 dane zaktualizowanego zamówienia przychodzącego
     * @throws ResponseStatusException 404 jeśli zamówienie przychodzące o podanym id nie istnieje
     */
    @PutMapping("/api/inbound-orders/{id}")
    public InboundOrder update(@RequestBody InboundOrder inboundOrder, @PathVariable Long id) {
        if (!inboundOrderRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        inboundOrder.setId(id);
        return inboundOrderRepository.save(inboundOrder);
    }
}
