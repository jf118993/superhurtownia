package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Role;
import pl.superhurtownia.backend.models.User;
import pl.superhurtownia.backend.models.WarehouseTeam;
import pl.superhurtownia.backend.repositories.RoleRepository;
import pl.superhurtownia.backend.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Kontroler użytkowników
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class UserController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    /**
     * Klasa z danymi do utworzenia użytkownika
     */
    static class UserRequest {
        public String name;
        public String password;
        public List<Long> roles;
    }


    /**
     * Konstruktor kontrolera użytkowników
     * @param userRepository Repozytorium użytkowników
     * @param passwordEncoder Enkoder haseł
     * @param roleRepository Repozytorium ról
     */
    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    /**
     * Zwraca wszystkich użytkowników
     * @return 200 lista wszystkich użytkowników
     */
    @GetMapping("/api/users")
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    /**
     * Zwraca użytkownika o podanym id
     * @param id id użytkownika
     * @return 200 użytkownik o podanym id
     */
    @GetMapping("/api/users/{id}")
    public User get(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

/*
    @PostMapping("/api/users")
    @ResponseStatus(HttpStatus.CREATED)
    public User create(@RequestBody User user) {
        user.setId(null);
        return userRepository.save(user);
    }*/

    /**
     * Tworzy nowego użytkownika
     * @param userRequest dane nowego użytkownika
     * @return 201 utworzony użytkownik
     * @throws RuntimeException jeśli rola o podanym id nie istnieje
     */
    @PostMapping("/api/users")
    @ResponseStatus(HttpStatus.CREATED)
    public User create(@RequestBody UserRequest userRequest) {
        User user = new User();
        user.setName(userRequest.name);
        user.setPassword(passwordEncoder.encode(userRequest.password));

        List<Role> roles = new ArrayList<>();
        for (Long roleId : userRequest.roles) {
            Role role = roleRepository.findById(roleId)
                    .orElseThrow(() -> new RuntimeException("Role with ID " + roleId + " not found"));
            roles.add(role);
        }
        user.setRoles(roles);

        return userRepository.save(user);
    }

    /**
     * Usuwa użytkownika o podanym id
     * @param id id użytkownika
     */
    @DeleteMapping("/api/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        userRepository.deleteById(id);
    }

    /**
     * Aktualizuje użytkownika o podanym id
     * @param id id użytkownika
     * @param userRequest dane użytkownika
     * @return 200 zaktualizowany użytkownik
     * @throws ResponseStatusException 404 jeśli użytkownik o podanym id nie istnieje
     * @throws RuntimeException jeśli rola o podanym id nie istnieje
     */
    @PutMapping("/api/users/{id}")
    public User update(@PathVariable Long id, @RequestBody UserRequest userRequest) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        user.setName(userRequest.name);

        if (userRequest.password != null && !userRequest.password.isEmpty()) {
            user.setPassword(passwordEncoder.encode(userRequest.password));
        }

        List<WarehouseTeam> existingWarehouseTeams = user.getWarehouseTeams();

        List<Role> updatedRoles = new ArrayList<>();
        for (Long roleId : userRequest.roles) {
            Role role = roleRepository.findById(roleId)
                    .orElseThrow(() -> new RuntimeException("Role with ID " + roleId + " not found"));
            updatedRoles.add(role);
        }

        user.setRoles(updatedRoles);
        user.setWarehouseTeams(existingWarehouseTeams);

        return userRepository.save(user);
    }
}
