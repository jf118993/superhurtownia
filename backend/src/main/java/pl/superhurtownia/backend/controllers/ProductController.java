package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Product;
import pl.superhurtownia.backend.repositories.ProductRepository;

/**
 * Kontroler produktów
 */
@CrossOrigin(origins = "http://localhost:5173",allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RestController
public class ProductController {
    private final ProductRepository productRepository;

    /**
     * Konstruktor kontrolera produktów
     * @param productRepository Repozytorium produktów
     */
    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Zwraca wszystkie produkty
     * @return 200 lista wszystkich produktów
     */
    @GetMapping("/api/products")
    public Iterable<Product> getAll() {
        return productRepository.findAll();
    }

    /**
     * Zwraca produkt o podanym id
     * @param id id produktu
     * @return 200 produkt o podanym id
     * @throws ResponseStatusException 404 jeśli produkt o podanym id nie istnieje
     */
    @GetMapping("/api/products/{id}")
    public Product get(@PathVariable Long id) {
        return productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Tworzy nowy produkt
     * @param product dane nowego produktu
     * @return 201 dane utworzonego produktu
     */
    @PostMapping("/api/products")
    @ResponseStatus(HttpStatus.CREATED)
    public Product add(@RequestBody Product product) {
        product.setId(null);
        return productRepository.save(product);
    }

    /**
     * Usuwa produkt o podanym id
     * @param id id produktu
     */
    @DeleteMapping("/api/products/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        productRepository.deleteById(id);
    }

    /**
     * Aktualizuje produkt o podanym id
     * @param id id produktu
     * @param product nowe dane produktu
     * @return 200 zaktualizowane dane produktu
     * @throws ResponseStatusException 404 jeśli produkt o podanym id nie istnieje
     */
    @PutMapping("/api/products/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Product update(@PathVariable Long id, @RequestBody Product product) {
        if (!productRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        product.setId(id);
        return productRepository.save(product);
    }
}