package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.User;
import pl.superhurtownia.backend.models.WarehouseTeam;
import pl.superhurtownia.backend.repositories.WarehouseTeamRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Kontroler zespołów magazynowych
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class WarehouseTeamController {
    private final WarehouseTeamRepository warehouseTeamRepository;

    /**
     * Konstruktor kontrolera zespołów magazynowych
     * @param warehouseTeamRepository Repozytorium zespołów magazynowych
     */
    @Autowired
    public WarehouseTeamController(WarehouseTeamRepository warehouseTeamRepository) {
        this.warehouseTeamRepository = warehouseTeamRepository;
    }

    /**
     * Zwraca wszystkie zespoły magazynowe
     * @return 200 lista wszystkich zespołów magazynowych
     */
    @GetMapping("/api/warehouse-teams")
    public Iterable<WarehouseTeam> getAll() {
        return warehouseTeamRepository.findAll();
    }

    /**
     * Zwraca zespół magazynowy o podanym id
     * @param id id zespołu magazynowego
     * @return 200 zespół magazynowy o podanym id
     */
    @GetMapping("/api/warehouse-teams/{id}")
    public WarehouseTeam get(@PathVariable Long id) {
        return warehouseTeamRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


    /*@PostMapping("/api/warehouse-teams")
    @ResponseStatus(HttpStatus.CREATED)
    public WarehouseTeam create(@RequestBody WarehouseTeam warehouseTeam) {
        warehouseTeam.setId(null);
        return warehouseTeamRepository.save(warehouseTeam);
    }*/

    /**
     * Tworzy nowy zespół magazynowy
     * @param requestBody dane zespołu magazynowego
     * @return 201 utworzony zespół magazynowy
     */
    @PostMapping("/api/warehouse-teams")
    @ResponseStatus(HttpStatus.CREATED)
    public WarehouseTeam create(@RequestBody Map<String, Object> requestBody) {
        WarehouseTeam warehouseTeam = new WarehouseTeam();
        warehouseTeam.setId(null);
        warehouseTeam.setName((String) requestBody.get("name"));

        List<Map<String, Object>> workersList = (List<Map<String, Object>>) requestBody.get("workers");
        List<User> workers = new ArrayList<>();
        if (workersList != null) {
            workers = workersList.stream()
                    .map(worker -> {
                        User user = new User();
                        Object idObject = worker.get("id");
                        if (idObject instanceof Integer) {
                            user.setId(((Integer) idObject).longValue());
                        } else if (idObject instanceof Long) {
                            user.setId((Long) idObject);
                        }
                        return user;
                    })
                    .collect(Collectors.toList());
        }

        warehouseTeam.setWorkers(workers);

        return warehouseTeamRepository.save(warehouseTeam);
    }

    /**
     * Usuwa zespół magazynowy o podanym id
     * @param id id zespołu magazynowego
     */
    @DeleteMapping("/api/warehouse-teams/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        warehouseTeamRepository.deleteById(id);
    }

    /*@PutMapping("/api/warehouse-teams/{id}")
    public WarehouseTeam update(@RequestBody WarehouseTeam warehouseTeam, @PathVariable Long id) {
        if (!warehouseTeamRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        warehouseTeam.setId(id);
        return warehouseTeamRepository.save(warehouseTeam);
    }*/

    /**
     * Aktualizuje zespół magazynowy o podanym id
     * @param requestBody dane zespołu magazynowego
     * @param id id zespołu magazynowego
     * @return 200 zaktualizowany zespół magazynowy
     * @throws ResponseStatusException 404 jeśli zespół magazynowy o podanym id nie istnieje
     */
    @PutMapping("/api/warehouse-teams/{id}")
    public WarehouseTeam update(@RequestBody Map<String, Object> requestBody, @PathVariable Long id) {
        if (!warehouseTeamRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        WarehouseTeam existingWarehouseTeam = warehouseTeamRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        String name = (String) requestBody.get("name");
        existingWarehouseTeam.setName(name);

        List<Map<String, Object>> workersList = (List<Map<String, Object>>) requestBody.get("workers");
        List<User> workers = new ArrayList<>();
        if (workersList != null) {
            workers = workersList.stream()
                    .map(worker -> {
                        User user = new User();
                        Object idObject = worker.get("id");
                        if (idObject instanceof Integer) {
                            user.setId(((Integer) idObject).longValue());
                        } else if (idObject instanceof Long) {
                            user.setId((Long) idObject);
                        }
                        return user;
                    })
                    .collect(Collectors.toList());
        }


        existingWarehouseTeam.setWorkers(workers);

        return warehouseTeamRepository.save(existingWarehouseTeam);
    }
}
