package pl.superhurtownia.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.services.DatabaseSeederService;
import pl.superhurtownia.backend.services.PdfCreationService;
import pl.superhurtownia.backend.services.SimpleWarehouseViewService;

import java.io.IOException;
import java.util.List;

/**
 * Kontroler z funkcjami pomocniczymi
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class UtilityController {

    /**
     * Klasa z danymi do utworzenia pdfa
     */
    static class PdfRequest {
        public String path;
        public Long orderId;
    }

    private final DatabaseSeederService databaseSeederService;
    private final SimpleWarehouseViewService simpleWarehouseViewService;
    private final PdfCreationService pdfCreationService;

    /**
     * Konstruktor kontrolera z funkcjami pomocniczymi
     * @param databaseSeederService serwis do zapełniania bazy danych przykładowymi danymi
     * @param simpleWarehouseViewService serwis do wyświetlania listy produktów wraz z ilością w magazynie
     * @param pdfCreationService serwis do tworzenia pdfów
     */
    public UtilityController(
            DatabaseSeederService databaseSeederService,
            SimpleWarehouseViewService simpleWarehouseViewService,
            PdfCreationService pdfCreationService
    ) {
        this.databaseSeederService = databaseSeederService;
        this.simpleWarehouseViewService = simpleWarehouseViewService;
        this.pdfCreationService = pdfCreationService;
    }

    /**
     * Zapełnia bazę danych przykładowymi danymi
     */
    @GetMapping("/api/dbseed")
    @ResponseStatus(HttpStatus.CREATED)
    public void seedDatabase() {
        databaseSeederService.seedDatabase();
    }

    /**
     * Zwraca listę produktów wraz z ilością w magazynie
     * @return 200 lista produktów wraz z ilością w magazynie
     */
    @GetMapping("/api/warehouse-view")
    public List<SimpleWarehouseViewService.ProductView> getSimpleWarehouseView() {
        return simpleWarehouseViewService.getProducts();
    }

    /**
     * Tworzy pdfa z zamówienia
     * @param pdfRequest dane do utworzenia pdfa
     */
    @PostMapping("/api/create-pdf")
    @ResponseStatus(HttpStatus.CREATED)
    public void createPdf(@RequestBody PdfRequest pdfRequest) {
        try {
            pdfCreationService.createPdf(pdfRequest.orderId, pdfRequest.path);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
