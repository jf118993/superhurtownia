package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.OutboundOrder;
import pl.superhurtownia.backend.repositories.OutboundOrderRepository;

import java.util.ArrayList;

/**
 * Kontroler zamówień wychodzących
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class OutboundOrderController {
    private final OutboundOrderRepository outboundOrderRepository;

    /**
     * Konstruktor kontrolera zamówień wychodzących
     * @param outboundOrderRepository Repozytorium zamówień wychodzących
     */
    @Autowired
    public OutboundOrderController(OutboundOrderRepository outboundOrderRepository) {
        this.outboundOrderRepository = outboundOrderRepository;
    }

    /**
     * Zwraca wszystkie zamówienia wychodzące
     * @return 200 lista wszystkich zamówień wychodzących
     */
    @GetMapping("/api/outbound-orders")
    public Iterable<OutboundOrder> getAll() {
        return outboundOrderRepository.findAll();
    }

    /**
     * Zwraca zamówienie wychodzące o podanym id
     * @param id id zamówienia wychodzącego
     * @return 200 zamówienie wychodzące o podanym id
     * @throws ResponseStatusException 404 jeśli nie znaleziono zamówienia wychodzącego o podanym id
     */
    @GetMapping("/api/outbound-orders/{id}")
    public OutboundOrder get(@PathVariable Long id) {
        return outboundOrderRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Tworzy nowe zamówienie wychodzące
     * @param outboundOrder dane nowego zamówienia wychodzącego
     * @return 201 dane utworzonego zamówienia wychodzącego
     */
    @PostMapping("/api/outbound-orders")
    @ResponseStatus(HttpStatus.CREATED)
    public OutboundOrder create(@RequestBody OutboundOrder outboundOrder) {
        outboundOrder.setId(null);
        return outboundOrderRepository.save(outboundOrder);
    }

    /**
     * Tworzy wiele nowych zamówień wychodzących
     * @param outboundOrders dane nowych zamówień wychodzących
     * @return 201 dane utworzonych zamówień wychodzących
     */
    @PostMapping("/api/outbound-orders/many")
    @ResponseStatus(HttpStatus.CREATED)
    public Iterable<OutboundOrder> createMany(@RequestBody Iterable<OutboundOrder> outboundOrders) {
        var ordersToAdd = new ArrayList<OutboundOrder>();
        for(var order: outboundOrders) {
            order.setId(null);
            ordersToAdd.add(outboundOrderRepository.save(order));
        }
        return ordersToAdd;
    }

    /**
     * Usuwa zamówienie wychodzące o podanym id
     * @param id id zamówienia wychodzącego
     */
    @DeleteMapping("/api/outbound-orders/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        outboundOrderRepository.deleteById(id);
    }

    /**
     * Aktualizuje zamówienie wychodzące o podanym id
     * @param outboundOrder dane zamówienia wychodzącego
     * @param id id zamówienia wychodzącego
     * @return 200 dane zaktualizowanego zamówienia wychodzącego
     * @throws ResponseStatusException 404 jeśli nie znaleziono zamówienia wychodzącego o podanym id
     */
    @PutMapping("/api/outbound-orders/{id}")
    public OutboundOrder update(@RequestBody OutboundOrder outboundOrder, @PathVariable Long id) {
        if (!outboundOrderRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        outboundOrder.setId(id);
        return outboundOrderRepository.save(outboundOrder);
    }
}
