package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Task;
import pl.superhurtownia.backend.models.User;
import pl.superhurtownia.backend.models.WarehouseTeam;
import pl.superhurtownia.backend.repositories.TaskRepository;
import pl.superhurtownia.backend.repositories.UserRepository;
import pl.superhurtownia.backend.repositories.WarehouseTeamRepository;

import java.util.*;

/**
 * Kontroler zadań
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class TaskController {
    private final TaskRepository taskRepository;
    private final WarehouseTeamRepository warehouseTeamRepository;
    private final UserRepository userRepository;

    /**
     * Konstruktor kontrolera zadań
     * @param taskRepository Repozytorium zadań
     * @param warehouseTeamRepository Repozytorium zespołów
     * @param userRepository Repozytorium użytkowników
     */
    @Autowired
    public TaskController(TaskRepository taskRepository, WarehouseTeamRepository warehouseTeamRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.warehouseTeamRepository = warehouseTeamRepository;
        this.userRepository = userRepository;
    }

    /**
     * Zwraca wszystkie zadania
     * @return 200 lista wszystkich zadań
     */
    @GetMapping("/api/tasks")
    public Iterable<Task> getAll() {
        return taskRepository.findAll();
    }

    /**
     * Zwraca zadanie o podanym id
     * @param id id zadania
     * @return 200 zadanie o podanym id
     * @throws ResponseStatusException 404 jeśli zadanie o podanym id nie istnieje
     */
    @GetMapping("/api/tasks/{id}")
    public Task get(@PathVariable Long id) {
        return taskRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Zwraca zadania przypisane do zespołu, do którego należy użytkownik, lub wszystkie jeśli użytkownik jest administratorem
     * @param authentication dane zalogowanego użytkownika
     * @return 200 lista zadań przypisanych do zespołu, do którego należy użytkownik
     */
    @GetMapping("/api/tasks-team")
    public Iterable<Task> getTeam(Authentication authentication) {
        String userName = authentication.getName();
        List<WarehouseTeam> userTeams = warehouseTeamRepository.findByWorkersName(userName);

        // Check if user is ROLE_ADMIN
        boolean isAdmin = hasRoleAdmin(userName);

        if (isAdmin) {
            // If user is ROLE_ADMIN, return all tasks
            return taskRepository.findAll();
        }

        if (userTeams.isEmpty()) {
            return Collections.emptyList();
        }

        WarehouseTeam warehouseTeam = userTeams.get(0);

        // Filter the tasks based on the team
        List<Task> teamTasks = new ArrayList<>();
        Iterable<Task> allTasks = taskRepository.findAll();
        for (Task task : allTasks) {
            if (task.getTeam() != null && task.getTeam().equals(warehouseTeam)) {
                teamTasks.add(task);
            }
        }

        return teamTasks;
    }

    private boolean hasRoleAdmin(String userName) {
        Optional<User> optionalUser = userRepository.findByName(userName);
        return optionalUser.isPresent() &&
                optionalUser.get().getRoles().stream()
                        .anyMatch(role -> role.getName().equals("ROLE_ADMIN"));
    }

    /**
     * Zwraca zadania z przypisanymi zespołami
     * @return 200 lista zadań z przypisanymi zespołami
     */
    @GetMapping("/api/tasks-with-teams")
    public List<Map<String, Object>> getTasksWithTeams() {
        Iterable<Task> tasks = taskRepository.findAll();
        List<Map<String, Object>> result = new ArrayList<>();

        for (Task task : tasks) {
            WarehouseTeam team = task.getTeam();
            if (team != null) {
                Map<String, Object> taskWithTeam = new HashMap<>();
                taskWithTeam.put("id", task.getId());
                taskWithTeam.put("status", task.getStatus());
                taskWithTeam.put("orderGroup", Collections.singletonMap("id", task.getOrderGroup().getId()));
                taskWithTeam.put("team", team.getName());
                result.add(taskWithTeam);
            }
        }

        return result;
    }

    /**
     * Zwraca zespół przypisany do zadania
     * @param taskId id zadania
     * @return 200 zespół przypisany do zadania
     * @throws ResponseStatusException 404 jeśli zadanie o podanym id nie istnieje
     */
    @GetMapping("/api/tasks-team/{taskId}")
    public WarehouseTeam getTaskTeam(@PathVariable Long taskId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Task not found"));

        return task.getTeam();
    }

    /**
     * Tworzy nowe zadanie
     * @param task zadanie do utworzenia
     * @return 201 utworzone zadanie
     */
    @PostMapping("/api/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public Task create(@RequestBody Task task) {
        task.setId(null);
        return taskRepository.save(task);
    }

    /**
     * Tworzy nowe zadanie i przypisuje je do zespołu, do którego należy użytkownik
     * @param authentication dane zalogowanego użytkownika
     * @param task zadanie do utworzenia
     * @return 201 utworzone zadanie
     * @throws IllegalArgumentException jeśli nazwa użytkownika jest pusta
     * @throws IllegalStateException jeśli użytkownik nie należy do żadnego zespołu lub zespół jest nullem
     */
    @PostMapping("/api/tasks-team")
    @ResponseStatus(HttpStatus.CREATED)
    public Task createWithTeam(Authentication authentication, @RequestBody Task task) {
        String userName = authentication.getName();
        if (userName == null || userName.isEmpty()) {
            throw new IllegalArgumentException("User name cannot be empty.");
        }

        List<WarehouseTeam> userTeams = warehouseTeamRepository.findByWorkersName(userName);
        if (userTeams == null || userTeams.isEmpty()) {
            throw new IllegalStateException("User does not belong to any warehouse team.");
        }

        WarehouseTeam warehouseTeam = userTeams.get(0);
        if (warehouseTeam == null) {
            throw new IllegalStateException("Warehouse team is null.");
        }

        task.setTeam(warehouseTeam);

        task.setId(null);
        return taskRepository.save(task);
    }

    /**
     * Usuwa zadanie o podanym id
     * @param id id zadania
     * @throws ResponseStatusException 404 jeśli zadanie o podanym id nie istnieje
     */
    @DeleteMapping("/api/tasks/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        taskRepository.deleteById(id);
    }

    /**
     * Aktualizuje zadanie o podanym id
     * @param task zadanie do aktualizacji
     * @param id id zadania
     * @return 200 zaktualizowane zadanie
     * @throws ResponseStatusException 404 jeśli zadanie o podanym id nie istnieje
     */
    @PutMapping("/api/tasks/{id}")
    public Task update(@RequestBody Task task, @PathVariable Long id) {
        if (!taskRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        task.setId(id);
        return taskRepository.save(task);
    }
}
