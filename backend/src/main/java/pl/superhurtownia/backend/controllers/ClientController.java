package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Client;
import pl.superhurtownia.backend.repositories.ClientRepository;

/**
 * Kontroler klientów
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class ClientController {
    private final ClientRepository clientRepository;

    /**
     * Konstruktor kontrolera klientów
     * @param clientRepository Repozytorium klientów
     */
    @Autowired
    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    /**
     * Zwraca wszystkich klientów
     * @return 200 lista wszystkich klientów
     */
    @GetMapping("/api/clients")
    public Iterable<Client> getAll() {
        return clientRepository.findAll();
    }

    /**
     * Zwraca klienta o podanym id
     * @param id id klienta
     * @return 200 klient o podanym id
     * @throws ResponseStatusException 404 jeśli klient o podanym id nie istnieje
     */
    @GetMapping("/api/clients/{id}")
    public Client get(@PathVariable Long id) {
        return clientRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Tworzy nowego klienta
     * @param client dane nowego klienta
     * @return 201 dane utworzonego klienta
     */
    @PostMapping("/api/clients")
    @ResponseStatus(HttpStatus.CREATED)
    public Client create(@RequestBody Client client) {
        client.setId(null);
        return clientRepository.save(client);
    }

    /**
     * Usuwa klienta o podanym id
     * @param id id klienta
     */
    @DeleteMapping("/api/clients/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        clientRepository.deleteById(id);
    }

    /**
     * Aktualizuje klienta o podanym id
     * @param client nowe dane klienta
     * @param id id klienta
     * @return 200 dane zaktualizowanego klienta
     * @throws ResponseStatusException 404 jeśli klient o podanym id nie istnieje
     */
    @PutMapping("/api/clients/{id}")
    public Client update(@RequestBody Client client, @PathVariable Long id) {
        if (!clientRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        client.setId(id);
        return clientRepository.save(client);
    }
}
