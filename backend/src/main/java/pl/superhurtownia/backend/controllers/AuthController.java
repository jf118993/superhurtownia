package pl.superhurtownia.backend.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.services.AuthService;

import java.util.NoSuchElementException;

/**
 * Kontroler autoryzacji
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class AuthController {

    /**
     * Klasa używana do przyjmowania danych logowania w formacie JSON
     */
    static class AuthRequest {
        public String username;
        public String password;
    }


    /**
     * Klasa używana do zwracania tokena w formacie JSON
     */
    public static class TokenResponse {
        public String token;

        public TokenResponse(String token) {
            this.token = token;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

    private final AuthService authService;

    /**
     * Konstruktor kontrolera autoryzacji
     * @param authService Serwis autoryzacji
     */
    public AuthController(AuthService authService) {
        this.authService = authService;
    }


    /*
    @PostMapping("/api/auth/token")
    @ResponseStatus(HttpStatus.CREATED)
    public String token(@RequestBody AuthRequest request) {
        LOG.debug("Generating token for {}", request.username);
        try {
            var token = authService.generateToken(request.username, request.password);
            LOG.debug("Generated token: {}", token);
            return token;
        } catch(NoSuchElementException e) {
            LOG.debug("User {} not found", request.username);
            throw new ResponseStatusException(org.springframework.http.HttpStatus.NOT_FOUND, "User not found");
        } catch(IllegalArgumentException e) {
            LOG.debug("Invalid password for user {}", request.username);
            throw new ResponseStatusException(org.springframework.http.HttpStatus.UNAUTHORIZED, "Invalid password");
        }
    }*/

    /**
     * Metoda przyjmująca ciało JSON klasy AuthRequest zawierająca dane logowania i zwracająca JSON klasy TokenResponse zawierająca token JWT
     * @param request dane logowania
     * @return response 200 zawierający token JWT
     * @throws ResponseStatusException 404 jeśli użytkownik nie istnieje, 401 jeśli hasło jest nieprawidłowe
     */
    @PostMapping("/api/auth/token")
    public ResponseEntity<TokenResponse> token(@RequestBody AuthRequest request) {
        LOG.debug("Generating token for {}", request.username);
        try {
            var token = authService.generateToken(request.username, request.password);
            LOG.debug("Generated token: {}", token);
            TokenResponse response = new TokenResponse(token);
            return ResponseEntity.ok(response);
        } catch (NoSuchElementException e) {
            LOG.debug("User {} not found", request.username);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        } catch (IllegalArgumentException e) {
            LOG.debug("Invalid password for user {}", request.username);
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
        }
    }
}
