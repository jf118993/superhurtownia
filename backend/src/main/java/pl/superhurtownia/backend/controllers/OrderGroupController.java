package pl.superhurtownia.backend.controllers;

import jakarta.transaction.Transactional;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.OrderGroup;
import pl.superhurtownia.backend.models.OutboundOrder;
import pl.superhurtownia.backend.repositories.OrderGroupRepository;
import pl.superhurtownia.backend.repositories.OutboundOrderRepository;
import pl.superhurtownia.backend.repositories.TaskRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Kontroler grup zamówień
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class OrderGroupController {

    /**
     * Klasa używana do zwracania grupy zamówień wraz z zamówieniami i sumą cen w formacie JSON
     */
    static class OrderGroupWithOrders {
        public Long id;
        public List<OutboundOrder> orders;
        public BigDecimal price;
    }
    private final OrderGroupRepository orderGroupRepository;
    private final OutboundOrderRepository outboundOrderRepository;
    private final TaskRepository taskRepository;

    /**
     * Konstruktor kontrolera grup zamówień
     * @param orderGroupRepository repozytorium grup zamówień
     * @param outboundOrderRepository repozytorium zamówień
     * @param taskRepository repozytorium zadań
     */
    public OrderGroupController(OrderGroupRepository orderGroupRepository, OutboundOrderRepository outboundOrderRepository, TaskRepository taskRepository) {
        this.orderGroupRepository = orderGroupRepository;
        this.outboundOrderRepository = outboundOrderRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Zwraca wszystkie grupy zamówień wraz z zamówieniami i sumą cen
     * @return 200 lista grup zamówień wraz z zamówieniami i sumą cen
     */
    @GetMapping("/api/order-groups")
    public Iterable<OrderGroupWithOrders> getAll() {
        var orderGroups = new ArrayList<OrderGroupWithOrders>();
        for (var orderGroup : orderGroupRepository.findAll()) {
            var orderGroupWithOrders = new OrderGroupWithOrders();
            orderGroupWithOrders.id = orderGroup.getId();
            orderGroupWithOrders.orders = outboundOrderRepository.findByOrderGroup(orderGroup);
            orderGroupWithOrders.price = orderGroupWithOrders.orders.stream().map(OutboundOrder::getCost).reduce(BigDecimal.ZERO, BigDecimal::add);
            orderGroups.add(orderGroupWithOrders);
        }
        return orderGroups;
    }

    /**
     * Zwraca grupę zamówień o podanym id wraz z zamówieniami i sumą cen
     * @param id id grupy zamówień
     * @return 200 grupa zamówień o podanym id wraz z zamówieniami i sumą cen
     * @throws ResponseStatusException jeśli grupa zamówień o podanym id nie istnieje
     */
    @GetMapping("/api/order-groups/{id}/orders")
    public Iterable<OutboundOrder> get(@PathVariable Long id) {
        var orderGroup = orderGroupRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return outboundOrderRepository.findByOrderGroup(orderGroup);
    }

    /**
     * Tworzy nową grupę zamówień
     * @return 201 dane utworzonej grupy zamówień
     */
    @PostMapping("/api/order-groups")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderGroup create() {
        return orderGroupRepository.save(new OrderGroup());
    }

    /**
     * Usuwa grupę zamówień o podanym id, i wszystkie zamówienia należące do niej oraz ich zadania
     * @param id id grupy zamówień
     * @throws ResponseStatusException 404 jeśli grupa zamówień o podanym id nie istnieje
     */
    @DeleteMapping("/api/order-groups/{id}/with-orders")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deleteWithOrders(@PathVariable Long id) {
        var orderGroup = orderGroupRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        var orders = outboundOrderRepository.findByOrderGroup(orderGroup);
        taskRepository.deleteAll(taskRepository.findByOrderGroup(orderGroup));
        outboundOrderRepository.deleteAll(orders);
        orderGroupRepository.delete(orderGroup);
    }
}
