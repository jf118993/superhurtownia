package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Role;
import pl.superhurtownia.backend.repositories.RoleRepository;

/**
 * Kontroler ról
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class RoleController {
    private final RoleRepository roleRepository;

    /**
     * Konstruktor kontrolera ról
     * @param roleRepository Repozytorium ról
     */
    @Autowired
    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * Zwraca wszystkie role
     * @return 200 lista wszystkich ról
     */
    @GetMapping("/api/roles")
    public Iterable<Role> getAll() {
        return roleRepository.findAll();
    }

    /**
     * Zwraca rolę o podanym id
     * @param id id roli
     * @return 200 rola o podanym id
     * @throws ResponseStatusException 404 jeśli rola o podanym id nie istnieje
     */
    @GetMapping("/api/roles/{id}")
    public Role get(@PathVariable Long id) {
        return roleRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
