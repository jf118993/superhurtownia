package pl.superhurtownia.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.superhurtownia.backend.models.Batch;
import pl.superhurtownia.backend.repositories.BatchRepository;

/**
 * Kontroler partii
 */
@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class BatchController {
    private final BatchRepository batchRepository;

    /**
     * Konstruktor kontrolera partii
     * @param batchRepository Repozytorium partii
     */
    @Autowired
    public BatchController(BatchRepository batchRepository) {
        this.batchRepository = batchRepository;
    }

    /**
     * Zwraca wszystkie partie
     * @return 200 lista wszystkich partii
     */
    @GetMapping("/api/batches")
    public Iterable<Batch> getAll() {
        return batchRepository.findAll();
    }

    /**
     * Zwraca partię o podanym id
     * @param id id partii
     * @return 200 partia o podanym id
     * @throws ResponseStatusException 404 jeśli partia o podanym id nie istnieje
     */
    @GetMapping("/api/batches/{id}")
    public Batch get(@PathVariable Long id) {
        return batchRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Tworzy nową partię
     * @param batch dane nowej partii
     * @return 201 dane utworzonej partii
     */
    @PostMapping("/api/batches")
    @ResponseStatus(HttpStatus.CREATED)
    public Batch create(@RequestBody Batch batch) {
        batch.setId(null);
        return batchRepository.save(batch);
    }

    /**
     * Usuwa partię o podanym id
     * @param id id partii
     */
    @DeleteMapping("/api/batches/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        batchRepository.deleteById(id);
    }

    /**
     * Aktualizuje partię o podanym id
     * @param batch nowe dane partii
     * @param id id partii do aktualizacji
     * @return 200 zaktualizowane dane partii
     */
    @PutMapping("/api/batches/{id}")
    public Batch update(@RequestBody Batch batch, @PathVariable Long id) {
        if (!batchRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        batch.setId(id);
        return batchRepository.save(batch);
    }

}
