package pl.superhurtownia.backend.services;

import org.springframework.stereotype.Service;
import pl.superhurtownia.backend.repositories.OrderGroupRepository;
import pl.superhurtownia.backend.repositories.OutboundOrderRepository;
import pl.superhurtownia.pdf.PdfBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Serwis tworzący pliki PDF
 */
@Service
public class PdfCreationService {
    private final OrderGroupRepository orderGroupRepository;
    private final OutboundOrderRepository outboundOrderRepository;

    /**
     * Konstruktor serwisu tworzącego pliki PDF
     * @param orderGroupRepository repozytorium grup zamówień
     * @param outboundOrderRepository repozytorium zamówień wychodzących
     */
    public PdfCreationService(OrderGroupRepository orderGroupRepository, OutboundOrderRepository outboundOrderRepository) {
        this.orderGroupRepository = orderGroupRepository;
        this.outboundOrderRepository = outboundOrderRepository;
    }

    /**
     * Tworzy plik PDF z fakturą dla grupy zamówień o podanym id
     * @param orderId id grupy zamówień
     * @param path ścieżka do pliku
     * @throws NoSuchElementException jeśli grupa zamówień o podanym id nie istnieje
     * @throws IOException jeśli wystąpi błąd podczas zapisu do pliku
     */
    public void createPdf(Long orderId, String path) throws NoSuchElementException, IOException {
        var orderGroup = orderGroupRepository.findById(orderId).orElseThrow();
        var outboundOrders = outboundOrderRepository.findByOrderGroup(orderGroup);
        PdfBuilder.makeInvoice(outboundOrders, "SuperHurtownia", new FileOutputStream(path));
    }
}
