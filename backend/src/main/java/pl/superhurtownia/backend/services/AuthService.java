package pl.superhurtownia.backend.services;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;
import pl.superhurtownia.backend.HurtowniaUserDetails;
import pl.superhurtownia.backend.repositories.UserRepository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Serwis do obsługi uwierzytelniania
 */
@Service
public class AuthService {

    private final JwtEncoder encoder;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    /**
     * Konstruktor serwisu uwierzytelniania
     * @param encoder enkoder JWT
     * @param passwordEncoder enkoder hasła
     * @param userRepository repozytorium użytkowników
     */
    public AuthService(JwtEncoder encoder, PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.encoder = encoder;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    /**
     * Generuje token JWT dla użytkownika o podanym loginie i haśle
     * @param username login użytkownika
     * @param password hasło użytkownika
     * @return token JWT
     * @throws NoSuchElementException jeśli użytkownik o podanym loginie nie istnieje
     * @throws IllegalArgumentException jeśli podane hasło jest nieprawidłowe
     */
    public String generateToken(String username, String password) throws NoSuchElementException, IllegalArgumentException {
        var user = userRepository.findByName(username).orElseThrow();
        if (!passwordEncoder.matches(password, user.getPassword()))
            throw new IllegalArgumentException("Invalid password");

        var authentication = new HurtowniaUserDetails(user, user.getRoles());
        var now = Instant.now();
        var scope = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(" "));
        var claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(now)
                .expiresAt(now.plus(1, ChronoUnit.HOURS))
                .subject(authentication.getUsername())
                .claim("scope", scope)
                .build();
        return this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }
}
