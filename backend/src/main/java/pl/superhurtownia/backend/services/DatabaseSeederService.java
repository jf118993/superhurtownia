package pl.superhurtownia.backend.services;

import com.github.javafaker.Faker;
import jakarta.transaction.Transactional;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.superhurtownia.backend.models.*;
import pl.superhurtownia.backend.repositories.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Klasa służąca do wypełniania bazy danych przykładowymi danymi
 */
@Service
@Transactional
public class DatabaseSeederService {
    private final BatchRepository batchRepository;
    private final ClientRepository clientRepository;
    private final InboundOrderRepository inboundOrderRepository;
    private final OutboundOrderRepository outboundOrderRepository;
    private final OrderGroupRepository orderGroupRepository;
    private final ProductRepository productRepository;
    private final RoleRepository roleRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final WarehouseTeamRepository warehouseTeamRepository;

    private final PasswordEncoder passwordEncoder;

    /**
     * Konstruktor serwisu wypełniającego bazę danych przykładowymi danymi
     * @param batchRepository repozytorium partii
     * @param clientRepository repozytorium klientów
     * @param inboundOrderRepository repozytorium zamówień przychodzących
     * @param outboundOrderRepository repozytorium zamówień wychodzących
     * @param orderGroupRepository repozytorium grup zamówień
     * @param productRepository repozytorium produktów
     * @param roleRepository repozytorium ról
     * @param taskRepository repozytorium zadań
     * @param userRepository repozytorium użytkowników
     * @param warehouseTeamRepository repozytorium zespołów magazynowych
     * @param passwordEncoder enkoder hasła
     */
    public DatabaseSeederService(
            BatchRepository batchRepository,
            ClientRepository clientRepository,
            InboundOrderRepository inboundOrderRepository,
            OutboundOrderRepository outboundOrderRepository,
            OrderGroupRepository orderGroupRepository,
            ProductRepository productRepository,
            RoleRepository roleRepository,
            TaskRepository taskRepository,
            UserRepository userRepository,
            WarehouseTeamRepository warehouseTeamRepository,
            PasswordEncoder passwordEncoder
    ) {
        this.batchRepository = batchRepository;
        this.clientRepository = clientRepository;
        this.inboundOrderRepository = inboundOrderRepository;
        this.outboundOrderRepository = outboundOrderRepository;
        this.orderGroupRepository = orderGroupRepository;
        this.productRepository = productRepository;
        this.roleRepository = roleRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.warehouseTeamRepository = warehouseTeamRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Metoda wywoływana przy starcie aplikacji, wypełnia bazę danych wymaganymi danymi do działania
     *
     * @param event zdarzenie kontekstu aplikacji
     */
    @EventListener
    public void initializeDatabase(ContextRefreshedEvent event) {
        var roleAdmin = roleRepository.findByName("ROLE_ADMIN").orElseGet(() -> {
            var role = new Role();
            role.setName("ROLE_ADMIN");
            return roleRepository.save(role);
        });

        if (roleRepository.findByName("ROLE_OFFICE").isEmpty()) {
            var role = new Role();
            role.setName("ROLE_OFFICE");
            roleRepository.save(role);
        }

        if (roleRepository.findByName("ROLE_MANAGER").isEmpty()) {
            var role = new Role();
            role.setName("ROLE_MANAGER");
            roleRepository.save(role);
        }

        if (roleRepository.findByName("ROLE_WAREHOUSE").isEmpty()) {
            var role = new Role();
            role.setName("ROLE_WAREHOUSE");
            roleRepository.save(role);
        }

        if (userRepository.findByName("admin").isEmpty()) {
            var user = new User();
            user.setName("admin");
            user.setPassword(passwordEncoder.encode("admin"));
            var userRoles = new ArrayList<Role>();
            userRoles.add(roleAdmin);
            user.setRoles(userRoles);
            userRepository.save(user);
        }
    }

    /**
     * Wypełnia bazę danych losowymi przykładowymi danymi
     */
    public void seedDatabase() {

        var faker = new Faker(new Locale("pl"));

        var orderGroups = new ArrayList<OrderGroup>();
        var products = new ArrayList<Product>();
        var users = new ArrayList<User>();
        var teams = new ArrayList<WarehouseTeam>();

        var roleOffice = roleRepository.findByName("ROLE_OFFICE").orElseGet(() -> {
            initializeDatabase(null);
            return roleRepository.findByName("ROLE_OFFICE").orElseThrow();
        });

        var roleManager = roleRepository.findByName("ROLE_MANAGER").orElseThrow();

        var roleWarehouse = roleRepository.findByName("ROLE_WAREHOUSE").orElseThrow();

        for (var i = 0; i < 5; i++) {
            var user = new User();
            user.setName(faker.name().username());
            user.setPassword(passwordEncoder.encode("password"));
            var userRoles = new ArrayList<Role>();
            userRoles.add(roleOffice);
            user.setRoles(userRoles);
            user = userRepository.save(user);
            users.add(user);
        }
        for (var i = 0; i < 5; i++) {
            var user = new User();
            user.setName(faker.name().username());
            user.setPassword(passwordEncoder.encode("password"));
            var userRoles = new ArrayList<Role>();
            userRoles.add(roleManager);
            user.setRoles(userRoles);
            user = userRepository.save(user);
            users.add(user);
        }
        for (var i = 0; i < 25; i++) {
            var user = new User();
            user.setName(faker.name().username());
            user.setPassword(passwordEncoder.encode("password"));
            var userRoles = new ArrayList<Role>();
            userRoles.add(roleWarehouse);
            user.setRoles(userRoles);
            user = userRepository.save(user);
            users.add(user);
        }

        /*
            user 0-4 - office
            user 5-9 - manager
            user 10-34 - warehouse
         */

        for (var i = 0; i < 5; i++) {
            var team = new WarehouseTeam();
            team.setName("Zespół " + i);

            var workers = new ArrayList<User>();
            workers.add(users.get(6 + i));
            for (var j = 0; j < 5; j++) {
                workers.add(users.get(10 + i * 5 + j));
            }

            team.setWorkers(workers);
            team = warehouseTeamRepository.save(team);
            teams.add(team);
        }

        for (var i = 0; i < 50; i++) {
            var product = new Product();
            product.setName(faker.food().fruit());
            product.setCost(BigDecimal.valueOf(faker.number().numberBetween(1, 1000), 2));
            product = productRepository.save(product);
            products.add(product);
        }
        for (var i = 0; i < 50; i++) {
            var product = new Product();
            product.setName(faker.food().vegetable());
            product.setCost(BigDecimal.valueOf(faker.number().numberBetween(1, 1000), 2));
            product = productRepository.save(product);
            products.add(product);
        }

        for (var i = 0; i < 100; i++) {
            var client = new Client();
            client.setName(faker.company().name());
            client.setAddress(faker.address().fullAddress());
            long nip;
            do {
                nip = faker.number().numberBetween(1000000000L, 9999999999L);
            } while (clientRepository.findByNip(nip).isPresent());
            client.setNip(nip);
            client = clientRepository.save(client);

            var orderGroup = orderGroupRepository.save(new OrderGroup());
            orderGroups.add(orderGroup);

            for (var j = 0; j < 2; j++) {
                var outboundOrder = new OutboundOrder();
                outboundOrder.setClient(client);
                outboundOrder.setProduct(products.get(faker.number().numberBetween(0, products.size() - 1)));
                outboundOrder.setAmount(faker.number().randomDouble(2, 1, 600));
                outboundOrder.setOrderGroup(orderGroup);
                outboundOrder.setOrderDate(
                        Instant.ofEpochMilli(faker.date().past(60, java.util.concurrent.TimeUnit.DAYS).getTime())
                                .atZone(ZoneId.systemDefault()).toLocalDateTime()
                );
                outboundOrder.setStatus(faker.options().option(OutboundOrder.OrderStatus.class));
                outboundOrder.calculateCost();
                outboundOrderRepository.save(outboundOrder);
            }
        }

        for (var i = 0; i < 100; i++) {
            var batch = new Batch();
            batch.setProduct(products.get(faker.number().numberBetween(0, products.size() - 1)));
            batch.setAmount(faker.number().randomDouble(2, 1, 600));
            batch.setDeliveryDate(
                    Instant.ofEpochMilli(faker.date().past(60, java.util.concurrent.TimeUnit.DAYS).getTime())
                            .atZone(ZoneId.systemDefault()).toLocalDateTime()
            );
            batch.setExpirationDate(
                    Instant.ofEpochMilli(faker.date().future(60, java.util.concurrent.TimeUnit.DAYS).getTime())
                            .atZone(ZoneId.systemDefault()).toLocalDateTime()
            );
            batchRepository.save(batch);
        }

        for (var i = 0; i < 100; i++) {
            var inboundOrder = new InboundOrder();
            inboundOrder.setProduct(products.get(faker.number().numberBetween(0, products.size() - 1)));
            inboundOrder.setAmount(faker.number().randomDouble(2, 1, 600));
            var orderDate = faker.date().past(15, java.util.concurrent.TimeUnit.DAYS);
            inboundOrder.setOrderDate(
                    Instant.ofEpochMilli(orderDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime()
            );
            inboundOrder.setStatus(faker.options().option(InboundOrder.OrderStatus.class));
            if (inboundOrder.getStatus() != InboundOrder.OrderStatus.NEW)
                inboundOrder.setDeliveryDate(Instant.ofEpochMilli(faker.date().between(orderDate, Date.from(Instant.now())).getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime());
            inboundOrderRepository.save(inboundOrder);
        }

        for (var orderGroup : orderGroups) {
            var orders = outboundOrderRepository.findByOrderGroup(orderGroup);

            var orderStatuses = orders.stream().map(OutboundOrder::getStatus);
            var status = Task.TaskStatus.IN_PROGRESS;
            if (orderStatuses.allMatch(orderStatus -> orderStatus == OutboundOrder.OrderStatus.DONE))
                status = Task.TaskStatus.DONE;
            else {
                orderStatuses = orders.stream().map(OutboundOrder::getStatus);
                if (orderStatuses.allMatch(orderStatus -> orderStatus == OutboundOrder.OrderStatus.NEW))
                    status = Task.TaskStatus.NEW;
            }
            var task = new Task();
            task.setStatus(status);
            task.setOrderGroup(orderGroup);
            task.setTeam(teams.get(faker.number().numberBetween(0, teams.size() - 1)));
            taskRepository.save(task);
        }
    }
}
