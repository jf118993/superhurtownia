package pl.superhurtownia.backend.services;

import org.springframework.stereotype.Service;
import pl.superhurtownia.backend.repositories.BatchRepository;
import pl.superhurtownia.backend.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Serwis do obsługi widoku magazynu
 */
@Service
public class SimpleWarehouseViewService {
    /**
     * Klasa pomocnicza do przechowywania danych o stanie produktu
     */
    public static class ProductView {
        public Long id;
        public String name;
        public Double amount;
        public BigDecimal price;
    }

    private final BatchRepository batchRepository;
    private final ProductRepository productRepository;

    /**
     * Konstruktor serwisu do obsługi widoku magazynu
     * @param batchRepository repozytorium partii
     * @param productRepository repozytorium produktów
     */
    public SimpleWarehouseViewService(BatchRepository batchRepository, ProductRepository productRepository) {
        this.batchRepository = batchRepository;
        this.productRepository = productRepository;
    }

    /**
     * Pobiera listę produktów wraz z ich stanem w magazynie
     * @return lista produktów
     */
    public List<ProductView> getProducts() {
        var productMap = new HashMap<Long, Double>();
        batchRepository.findAll().forEach(batch -> {
            var productId = batch.getProduct().getId();
            var amount = batch.getAmount();
            if (productMap.containsKey(productId)) {
                productMap.put(productId, productMap.get(productId) + amount);
            } else {
                productMap.put(productId, amount);
            }
        });

        return productMap.entrySet().stream().map(entry -> {
            var productView = new ProductView();
            productView.id = entry.getKey();
            var product = productRepository.findById(entry.getKey());
            productView.name = product.isPresent() ? product.get().getName() : "Brak danych";
            productView.amount = entry.getValue();
            productView.price = product.isPresent() ? product.get().getCost() : BigDecimal.ZERO;
            return productView;
        }).toList();
    }
}
