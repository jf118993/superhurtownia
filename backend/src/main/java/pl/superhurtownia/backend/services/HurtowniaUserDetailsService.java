package pl.superhurtownia.backend.services;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.superhurtownia.backend.HurtowniaUserDetails;
import pl.superhurtownia.backend.repositories.UserRepository;

/**
 * Serwis danych użytkowników
 */
@Service
public class HurtowniaUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    /**
     * Konstruktor serwisu danych użytkowników
     * @param userRepository repozytorium użytkowników
     */
    @Autowired
    public HurtowniaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Znajduje użytkownika po nazwie
     * @param username nazwa użytkownika
     * @return dane użytkownika
     * @throws UsernameNotFoundException jeśli użytkownik o podanej nazwie nie istnieje
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByName(username);
        if (user.isEmpty()) throw new UsernameNotFoundException(username);
        return new HurtowniaUserDetails(user.get(), user.get().getRoles());
    }
}
