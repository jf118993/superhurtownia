package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.OrderGroup;
import pl.superhurtownia.backend.models.Task;

import java.util.List;

/**
 * Interfejs repozytorium zadań
 */
public interface TaskRepository extends CrudRepository<Task, Long> {
    /**
     * Znajduje zadania po statusie
     * @param status status zadania
     * @return lista zadań
     */
    List<Task> findByStatus(Task.TaskStatus status);

    /**
     * Znajduje zadania po grupie zamówień
     * @param orderGroup grupa zamówień
     * @return lista zadań
     */
    List<Task> findByOrderGroup(OrderGroup orderGroup);

    /**
     * Znajduje zadania po grupie zamówień i statusie
     * @param teamId id zespołu
     * @return lista zadań
     */
    Iterable<Task> findAllByTeamId(Long teamId);
}
