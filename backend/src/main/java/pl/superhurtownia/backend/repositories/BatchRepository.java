package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Batch;
import pl.superhurtownia.backend.models.Product;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interfejs repozytorium partii
 */
public interface BatchRepository extends CrudRepository<Batch, Long> {
    /**
     * Znajduje partię po produkcie
     * @param product produkt
     * @return lista partii
     */
    List<Batch> findByProduct(Product product);

    /**
     * Znajduje partie po dacie ważności pomiędzy podanymi
     * @param from od
     * @param to do
     * @return lista partii
     */
    List<Batch> findByExpirationDateBetween(LocalDateTime from, LocalDateTime to);

    /**
     * Znajduje partie z datą ważności przed podaną
     * @param date data
     * @return lista partii
     */
    List<Batch> findByExpirationDateBefore(LocalDateTime date);

    /**
     * Znajduje partie z datą ważności po podanej
     * @param date data
     * @return lista partii
     */
    List<Batch> findByExpirationDateAfter(LocalDateTime date);
}
