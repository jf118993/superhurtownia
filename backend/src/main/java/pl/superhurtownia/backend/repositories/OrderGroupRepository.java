package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.OrderGroup;

/**
 * Interfejs repozytorium grup zamówień
 */
public interface OrderGroupRepository extends CrudRepository<OrderGroup, Long> {
}
