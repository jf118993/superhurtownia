package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Product;

import java.util.Optional;

/**
 * Interfejs repozytorium produktów
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
    /**
     * Znajduje produkt po nazwie
     * @param name nazwa produktu
     * @return produkt
     */
    Optional<Product> findByName(String name);
}
