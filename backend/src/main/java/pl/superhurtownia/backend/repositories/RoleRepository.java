package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Role;

import java.util.Optional;

/**
 * Interfejs repozytorium ról
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
    /**
     * Znajduje rolę po nazwie
     * @param name nazwa roli
     * @return rola
     */
    Optional<Role> findByName(String name);
}
