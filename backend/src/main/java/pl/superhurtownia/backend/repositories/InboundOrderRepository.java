package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.InboundOrder;
import pl.superhurtownia.backend.models.Product;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interfejs repozytorium zamówień przychodzących
 */
public interface InboundOrderRepository extends CrudRepository<InboundOrder, Long> {
    /**
     * Znajduje zamówienia przychodzące dla danego produktu
     * @param product produkt
     * @return lista zamówień
     */
    List<InboundOrder> findByProduct(Product product);

    /**
     * Znajduje zamówienia przychodzące o podanym statusie
     * @param status status
     * @return lista zamówień
     */
    List<InboundOrder> findByStatus(InboundOrder.OrderStatus status);

    /**
     * Znajduje zamówienia z datą zamówienia pomiędzy podanymi
     * @param from od
     * @param to do
     * @return lista zamówień
     */
    List<InboundOrder> findByOrderDateBetween(LocalDateTime from, LocalDateTime to);

    /**
     * Znajduje zamówienia z datą zamówienia przed podaną
     * @param date data
     * @return lista zamówień
     */
    List<InboundOrder> findByOrderDateBefore(LocalDateTime date);

    /**
     * Znajduje zamówienia z datą zamówienia po podanej
     * @param date data
     * @return lista zamówień
     */
    List<InboundOrder> findByOrderDateAfter(LocalDateTime date);
}
