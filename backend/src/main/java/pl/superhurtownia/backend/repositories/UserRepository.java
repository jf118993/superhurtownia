package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Role;
import pl.superhurtownia.backend.models.User;

import java.util.List;
import java.util.Optional;

/**
 * Interfejs repozytorium użytkowników
 */
public interface UserRepository extends CrudRepository<User, Long>{
    /**
     * Znajduje użytkownika po nazwie
     * @param name nazwa użytkownika
     * @return użytkownik
     */
    Optional<User> findByName(String name);

    /**
     * Znajduje użytkowników po roli
     * @param role rola użytkownika
     * @return użytkownik
     */
    List<User> findByRolesContaining(Role role);

}
