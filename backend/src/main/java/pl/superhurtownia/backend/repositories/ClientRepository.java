package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Client;

import java.util.Optional;

/**
 * Interfejs repozytorium klientów
 */
public interface ClientRepository extends CrudRepository<Client, Long> {
    /**
     * Znajduje klienta po nazwie
     * @param name nazwa klienta
     * @return klient
     */
    Optional<Client> findByName(String name);

    /**
     * Znajduje klienta po adresie
     * @param address adres klienta
     * @return klient
     */
    Optional<Client> findByAddress(String address);

    /**
     * Znajduje klienta po numerze nip
     * @param nip numer nip klienta
     * @return klient
     */
    Optional<Object> findByNip(long nip);
}
