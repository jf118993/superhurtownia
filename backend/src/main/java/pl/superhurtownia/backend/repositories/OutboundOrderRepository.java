package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.Client;
import pl.superhurtownia.backend.models.OrderGroup;
import pl.superhurtownia.backend.models.OutboundOrder;
import pl.superhurtownia.backend.models.Product;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interfejs repozytorium zamówień wychodzących
 */
public interface OutboundOrderRepository extends CrudRepository<OutboundOrder, Long> {
    /**
     * Znajduje zamówienia wychodzące dla danego produktu
     * @param product produkt
     * @return lista zamówień
     */
    List<OutboundOrder> findByProduct(Product product);

    /**
     * Znajduje zamówienia wychodzące o podanym statusie
     * @param status status
     * @return lista zamówień
     */
    List<OutboundOrder> findByStatus(OutboundOrder.OrderStatus status);

    /**
     * Znajduje zamówienia dla danego klienta
     * @param client klient
     * @return lista zamówień
     */
    List<OutboundOrder> findByClient(Client client);

    /**
     * Znajduje zamówienia z datą zamówienia pomiędzy podanymi
     * @param from od
     * @param to do
     * @return lista zamówień
     */
    List<OutboundOrder> findByOrderDateBetween(LocalDateTime from, LocalDateTime to);

    /**
     * Znajduje zamówienia z datą zamówienia przed podaną
     * @param date data
     * @return lista zamówień
     */
    List<OutboundOrder> findByOrderDateBefore(LocalDateTime date);

    /**
     * Znajduje zamówienia z datą zamówienia po podanej
     * @param date data
     * @return lista zamówień
     */
    List<OutboundOrder> findByOrderDateAfter(LocalDateTime date);

    /**
     * Znajduje zamówienia dla podanej grupy zamówień
     * @param orderGroup grupa zamówień
     * @return lista zamówień
     */
    List<OutboundOrder> findByOrderGroup(OrderGroup orderGroup);
}
