package pl.superhurtownia.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.superhurtownia.backend.models.WarehouseTeam;

import java.util.List;

/**
 * Interfejs repozytorium zespołów magazynowych
 */
public interface WarehouseTeamRepository extends CrudRepository<WarehouseTeam, Long> {
    /**
     * Znajduje zespoły po nazwie
     * @param name nazwa zespołu
     * @return lista zespołów
     */
    List<WarehouseTeam> findByName(String name);

    /**
     * Znajduje zespoły po nazwie pracownika
     * @param userName nazwa pracownika
     * @return lista zespołów
     */
    List<WarehouseTeam> findByWorkersName(String userName);
}
