package pl.superhurtownia.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.superhurtownia.backend.configurations.RsaKeyProperties;

/**
 * Klasa główna aplikacji
 */
@EnableConfigurationProperties(RsaKeyProperties.class)
@SpringBootApplication
public class BackendApplication {
	/**
	 * Metoda główna aplikacji
	 * @param args argumenty
	 */
	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

}
