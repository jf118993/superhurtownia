package pl.superhurtownia.pdf;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.UnitValue;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Klasa do tworzenia plików PDF.
 */
public class PdfBuilder {
    private static class Order {
        String productName;
        Double amount;
        BigDecimal cost;
    }

    /**
     * Tworzy PDF faktury na podstawie kolekcji zamówień i zapisuje go do podanego strumienia.
     * @param orders kolekcja zamówień pl.superhurtownia.model.pl.superhurtownia.pdf.OutboundOrder
     * @param seller nazwa sprzedawcy
     * @param outputStream strumień do zapisu
     * @throws IOException jeśli wystąpi błąd podczas zapisu do strumienia
     */
    @SuppressWarnings("UnnecessaryUnicodeEscape")
    public static void makeInvoice(Collection<?> orders, String seller, OutputStream outputStream) throws IOException {
        var _orders = orders.stream().map((order) -> {
            var orderClass = order.getClass();
            var _order = new Order();
            for (var field : orderClass.getDeclaredFields()) {
                try {
                    field.setAccessible(true);
                    switch (field.getName()) {
                        case "product" -> {
                            var product = field.get(order);
                            var productClass = product.getClass();
                            for (var productField : productClass.getDeclaredFields()) {
                                productField.setAccessible(true);
                                if (productField.getName().equals("name"))
                                    _order.productName = (String) productField.get(product);
                            }
                        }
                        case "amount" -> _order.amount = (Double) field.get(order);
                        case "cost" -> _order.cost = (BigDecimal) field.get(order);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    throw new IllegalArgumentException("Cannot access field " + field.getName());
                }
            }
            return _order;
        }).toList();

        var anyOrder = orders.stream().findAny().orElseThrow(() -> new IllegalArgumentException("Cannot find order"));

        Object client;
        try {
            var clientField =Arrays.stream(anyOrder.getClass().getDeclaredFields())
                    .filter((field) -> field.getName().equals("client"))
                    .findAny().orElseThrow(() -> new IllegalArgumentException("Cannot find client field"));
            clientField.setAccessible(true);
            client = clientField.get(anyOrder);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Cannot access client field", e);
        }
        var clientClass = client.getClass();
        String clientName = null;
        String clientAddress = null;
        Long clientNip = null;
        for (var field : clientClass.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                switch (field.getName()) {
                    case "name" -> clientName = (String) field.get(client);
                    case "address" -> clientAddress = (String) field.get(client);
                    case "nip" -> clientNip = (Long) field.get(client);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new IllegalArgumentException("Cannot access field " + field.getName(), e);
            }
        }

        var builder = new PdfBuilder(outputStream);
        builder
                .header("Faktura")
                .text("Sprzedawca: " + seller)
                .text("")
                .text("Nabywca: " + clientName)
                .text("Adres: " + clientAddress)
                .text("NIP: " + clientNip)
                .text("")
                .text(
                        "Data wystawienia: " +
                                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"))
                )
                .text("");
        builder.table(
                Arrays.asList("L.p.", "Nazwa", "Ilość", "Cena"),
                _orders.stream().map((order) -> Arrays.asList(
                    Integer.toString(_orders.indexOf(order) + 1),
                    order.productName,
                    order.amount.toString() + " kg",
                    order.cost.toString() + " zł"
                )).toList()
        );

        builder.text("")
                .text("Razem: " + _orders.stream().map((order) -> order.cost).reduce(BigDecimal.ZERO, BigDecimal::add) + " zł")
                .text("Podpis osoby upowa\u017cnionej do wystawienia faktury: ________________________")
                .text("Podpis osoby upowa\u017cnionej do odbioru faktury: ________________________");
        builder.save();
    }

//    Nie wiem jakim cudem to działa, ale działa
    @SuppressWarnings({"DataFlowIssue", "UnnecessaryUnicodeEscape"})
    private static String replacePolishChars(String input) {
        return input.replace("ą", "\u0105")
                .replace("ć", "\u0107")
                .replace("ę", "\u0119")
                .replace("ł", "\u0142")
                .replace("ń", "\u0144")
                .replace("ó", "\u00f3")
                .replace("ś", "\u015b")
                .replace("ż", "\u017c")
                .replace("ź", "\u017a");
    }

    private final Document document;
    private final PdfFont regularFont;
    private final PdfFont boldFont;

    /**
     * @param outputStream strumień wyjściowy, do którego zostanie zapisany plik PDF
     * @throws IOException jeśli wystąpi błąd przy odczycie czcionek
     * @throws NullPointerException jeśli brakuje plików czcionek
     */
    public PdfBuilder(OutputStream outputStream) throws IOException {
        var pdfDocument = new PdfDocument(new PdfWriter(outputStream));
        document = new Document(pdfDocument);
        var boldFontStream = Objects.requireNonNull(getClass().getResourceAsStream("/NotoSans-Bold.ttf"));
        boldFont = PdfFontFactory.createFont(boldFontStream.readAllBytes(), PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
        boldFontStream.close();
        var regularFontStream = Objects.requireNonNull(getClass().getResourceAsStream("/NotoSans-Regular.ttf"));
        regularFont = PdfFontFactory.createFont(regularFontStream.readAllBytes(), PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
        regularFontStream.close();
    }

    /**
     * Zapisuje tekst nagłówkowy do pliku PDF
     * @param header tekst nagłówkowy
     * @return obiekt klasy pl.superhurtownia.pdf.PdfBuilder
     */
    public PdfBuilder header(String header) {
        document.add(
                new Paragraph(replacePolishChars(header))
                        .setFont(boldFont)
                        .setFontSize(24)
        );
        return this;
    }

    /**
     * Zapisuje tekst do pliku PDF
     * @param text tekst
     * @return obiekt klasy pl.superhurtownia.pdf.PdfBuilder
     */
    public PdfBuilder text(String text) {
        document.add(
                new Paragraph(replacePolishChars(text))
                        .setFont(regularFont)
                        .setFontSize(12)
        );
        return this;
    }

    /**
     * Zapisuje tabelę do pliku PDF
     * @param header nagłówki kolumn
     * @param table komórki tabeli
     * @return obiekt klasy pl.superhurtownia.pdf.PdfBuilder
     */
    public PdfBuilder table(List<String> header, List<List<String>> table) {
        var tableModel = new Table(4).setFont(regularFont).setFontSize(12);
        tableModel.setWidth(UnitValue.createPercentValue(100));
        for (var cell : header) {
            tableModel.addHeaderCell(replacePolishChars(cell));
        }
        for (var row : table) {
            for (var cell : row) {
                tableModel.addCell(replacePolishChars(cell));
            }
        }
        document.add(tableModel);
        return this;
    }

    /**
     * Dodaje nową stronę do pliku PDF
     * @return obiekt klasy pl.superhurtownia.pdf.PdfBuilder
     */
    public PdfBuilder newPage() {
        document.add(new AreaBreak());
        return this;
    }

    /**
     * Zapisuje plik PDF
     */
    public void save() {
        document.close();
    }
}